import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Lebah here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Lebah extends Actor
{
    /**
     * Act - do whatever the Lebah wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
       gerak();
       kenaLalat();
       
       MyWorld dunia = (MyWorld)getWorld();
       getWorld().showText("JUMLAH LALAT : "+dunia.getSkorLebah(),100,30);
       
       
    }    
    public void gerak(){
     if (Greenfoot.isKeyDown("Up")) {
            setLocation(getX(),getY() - 10);
        }
        if (Greenfoot.isKeyDown("down")) {
            setLocation(getX(),getY() + 10);
        }
        if (Greenfoot.isKeyDown("right")) {
            setLocation(getX()+5,getY());
        }
        if (Greenfoot.isKeyDown("left")) {
            setLocation(getX()-5,getY());
        }
        if (Greenfoot.isKeyDown("space")) {
            setRotation(getRotation()+30);  
        }
    }
    
    public void kenaLalat(){
    if (isTouching(Lalat.class)){
        removeTouching(Lalat.class);
        MyWorld dunia = (MyWorld)getWorld();
        dunia.skor();
    }
    }
        
}
