import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class MyWorld here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class MyWorld extends World
{
    private int skorLebah = 10;
  
    /**
     * Constructor for objects of class MyWorld.
     * 
     */
    public MyWorld()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(600, 400, 1); 
        prepare();
    }
    
   
    private void prepare(){
        Lebah lebah = new Lebah();
        addObject(lebah,55,214);
        
     
        Lalat lalat = new Lalat();
        addObject(lalat,34,343);
        Lalat lalat1 = new Lalat();
        addObject(lalat1,84,343);
         Lalat lalat2 = new Lalat();
        addObject(lalat2,134,343);
        Lalat lalat3 = new Lalat();
        addObject(lalat3,184,343);
         Lalat lalat4 = new Lalat();
        addObject(lalat4,234,343);
        Lalat lalat5 = new Lalat();
        addObject(lalat5,284,343);
         Lalat lalat6 = new Lalat();
        addObject(lalat6,334,343);
        Lalat lalat7 = new Lalat();
        addObject(lalat7,384,343);
         Lalat lalat8 = new Lalat();
        addObject(lalat8,434,343);
        Lalat lalat9 = new Lalat();
        addObject(lalat9,484,343);
    }
    
    public int getSkorLebah(){
    return skorLebah;
    }
   public void skor(){
       skorLebah--;
    }
        

  

    

}
